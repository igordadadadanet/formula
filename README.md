# TeldaFormulas

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.1.

# Страница на Pages

https://igordadadadanet.gitlab.io/formula/

## To-do:

+ ~~совпадение по буквам~~
+ ~~навигация кнопками~~
+ ~~закрывающиеся скобки~~

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change
any of the source files.

## Что я применял

+ Регулярные выражения;
+ Корректная обработка спец символов, введенных в input;
+ Обработку некоторых действий отправлял в очередь макрозадач (вроде назначения setSelectionRange после ввода данных);
+ Сервисы, пайпы, директивы, реактивные формы;
