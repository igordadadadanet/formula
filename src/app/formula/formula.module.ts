import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";

import { FormulaContainerComponent } from './formula-container/formula-container.component';
import { DropdownMenuComponent } from './formula-container/dropdown-menu/dropdown-menu.component';
import { FormulaDataService } from "./services/formula-data.service";
import { RegularExpressionService } from "./services/regular-expression.service";
import { SearchBoldPipe } from './pipes/search-bold.pipe';
import { InputDirective } from './directives/input.directive';


@NgModule({
  declarations: [
    FormulaContainerComponent,
    DropdownMenuComponent,
    SearchBoldPipe,
    InputDirective
  ],
  exports: [
    FormulaContainerComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  providers: [
    FormulaDataService,
    RegularExpressionService
  ]
})
export class FormulaModule {
}
