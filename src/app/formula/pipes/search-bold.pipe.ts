import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'searchBold'
})
export class SearchBoldPipe implements PipeTransform {
  transform(value: string, currentInputValue: string): string {
    const regExp = new RegExp(currentInputValue, 'gi');
    return value.replace(regExp, subStr => subStr.bold());

  }
}
