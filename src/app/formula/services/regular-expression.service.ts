import { Injectable } from '@angular/core';


@Injectable()
export class RegularExpressionService {
  public readonly _inputStringToArrayRegExp: RegExp = /(\w+)|(<>|!=|&&|<=|>=|\|\|)|([()])|([+=\-\/<>{}])/gmi;
  public readonly _specSymbols: string[] = ['[', ']', '^', '$', '.', '|', '?', '*', '+', '(', ')'];
  public readonly _regExpStables: RegExp = /(\((?!\w)(?!\())/y;

}
