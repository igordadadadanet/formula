import { Injectable } from '@angular/core';
import { RegularExpressionService } from "./regular-expression.service";


@Injectable()
export class FormulaDataService {
  public readonly _mathOperators: string[];
  public readonly _booleanOperators: string[];
  public readonly _functions: string[];
  public readonly _constants: string[];

  constructor(private regExpService: RegularExpressionService) {
    this._mathOperators = ['+', '-', '*', '/', '%', '^'];

    this._booleanOperators = ['!=', '=', '<>', '<', '<=', '>', '>=', '&&', '||'];

    this._functions = [
      'NOT',
      'IF',
      'RANDOM',
      'MIN',
      'MAX',
      'ABS',
      'ROUND',
      'FLOOR',
      'CEILING',
      'LOG',
      'LOG10',
      'SQRT',
      'SINR',
      'COSR',
      'TANR',
      'COTR',
      'SECR',
      'CSCR',
      'ASINR',
      'ACOSR',
      'ATANR',
      'ACOTR',
      'ATAN2R',
      'SIN',
      'COS',
      'TAN',
      'COT',
      'SEC',
      'CSC',
      'ASIN',
      'ACOS',
      'ATAN',
      'ACOT',
      'ATAN2',
      'SINH',
      'COSH',
      'TANH',
      'COTH',
      'SECH',
      'CSCH',
      'ASINH',
      'ACOSH',
      'ATANH',
      'RAD',
      'DEG',
      'FACT'
    ];

    this._constants = ['e', 'PI', 'TRUE', 'FALSE', 'NULL'];

  }

  get allValues() {
    return [...this._booleanOperators, ...this._functions, ...this._constants];

  }

  public findMatches(value: string): string[] {//
    value = value.trim(); //убираем пробел из введенного значения
    if (!value.length || value == '||') return [];

    if (this.regExpService._specSymbols.includes(value)) {
      value = `\\${value}`

    }

    try {
      const regExp: RegExp = new RegExp(value, 'gi');
      return this.allValues.filter((str: string) => str.match(regExp) && str.toLowerCase() !== value.toLowerCase())
        .sort((a: string, b: string) => { //сначала сортировка по алфавиту
          return a.localeCompare(b)
        })
        .sort((a: string, b: string) => {
          return a.toLowerCase().indexOf(value.toLowerCase()) - b.toLowerCase().indexOf(value.toLowerCase()); //сортировка массива по буквенным совпадениям
        });

    } catch (err) {
      return [];

    }
  }
}
