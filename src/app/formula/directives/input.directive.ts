import { Directive, ElementRef, Input } from '@angular/core';
import { RegularExpressionService } from "../services/regular-expression.service";
import { asyncScheduler } from "rxjs";
import { FormControl } from "@angular/forms";


@Directive({
  selector: '[appInput]'
})
export class InputDirective {
  @Input('formControl') public formControl!: FormControl;
  public currentInputValue: string = '';

  constructor(private input: ElementRef, private regExpService: RegularExpressionService) {
  }

  inputValueChanges(inputValue: string) {
    const allRegExp: RegExp = this.regExpService._inputStringToArrayRegExp;
    const arrayOfValues: string[] = inputValue.split(allRegExp).filter(str => str); //делим значение инпут на массив логических значений
    this.getActiveInputValue(arrayOfValues); //определяем с каким значением на данный момент работает пользователь

  }

  getActiveInputValue(inputValueArray: string[]): void {//метод для определения активного значения в инпут строке, с которым работает пользователь
    let counter: number = 0;
    let activeInputValue: string = '';

    for (let value of inputValueArray) {
      counter += value.length;

      if (this.input.nativeElement.selectionStart <= counter) {
        activeInputValue = value;
        break;

      }
    }
    this.currentInputValue = activeInputValue;

  }

  replaceString(substr: string, functions: string[]): void {
    let replacementString: string; //строка, которая будет заменять входящее значение
    let placeBetweenParentheses: number; //положение каретки после замены значения

    const completeString: string = this.input.nativeElement.value; //вся инпут строка
    const currentIndex: number = completeString.lastIndexOf(this.currentInputValue); //с какого индекса начинать замену текста

    let substrRegExp: RegExp = new RegExp(this.currentInputValue, 'iy'); //создаем регулярное выражение со значением субстроки на замену
    if (this.currentInputValue === '|') substrRegExp = /\|/;
    substrRegExp.lastIndex = currentIndex; //устанавливаем текущий индекс в регулярное выражение

    if (functions.includes(substr)) { //если заменяемое значение является функцией
      const staplesRegExp: RegExp = new RegExp(`${this.currentInputValue}\\(\\)`, 'i');
      placeBetweenParentheses = currentIndex + substr.length + 1;

      staplesRegExp.exec(completeString) ? replacementString = substr : replacementString = `${substr}()`; //ставим скобки после функции там, где их нет

    } else {
      placeBetweenParentheses = currentIndex + substr.length;
      replacementString = substr;

    }
    this.render(placeBetweenParentheses, substrRegExp, replacementString)

  }

  setStaples() {
    asyncScheduler.schedule(() => {
      const currentIndex: number = this.input.nativeElement.selectionStart;

      const regExpStables: RegExp = this.regExpService._regExpStables;
      regExpStables.lastIndex = currentIndex - 1;

      this.render(currentIndex, regExpStables, '()');
    }, 0);

  }

  render(placeBetweenParentheses: number, regExp: RegExp, replacementString: string) {
    const completeString: string = this.input.nativeElement.value;

    this.input.nativeElement.focus();
    asyncScheduler.schedule(() => this.input.nativeElement.setSelectionRange(placeBetweenParentheses, placeBetweenParentheses), 0);

    this.formControl.setValue(completeString.replace(regExp, replacementString));

  }
}
