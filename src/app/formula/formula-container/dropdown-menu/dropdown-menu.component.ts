import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { asyncScheduler, BehaviorSubject, Subscription } from "rxjs";
import { ActiveValue } from "../../interfaces/interfaces";


@Component({
  selector: 'app-dropdown-menu',
  templateUrl: './dropdown-menu.component.html',
  styleUrls: ['./dropdown-menu.component.scss']
})
export class DropdownMenuComponent implements OnInit, OnDestroy {
  @Input() public options!: string[];
  @Input() public activeOption!: BehaviorSubject<ActiveValue>;
  private activeOptionSubscription!: Subscription;
  @Input() public currentInputValue: string = '';

  @ViewChild('dropdownList') private dropdownList!: ElementRef;

  @Output() public optionSelect: EventEmitter<string> = new EventEmitter<string>();

  setOption(value: string) {
    this.optionSelect.emit(value);

  }

  ngOnInit() {
    this.activeOptionSubscription = this.activeOption.subscribe(({num, keyBoardNav}: ActiveValue) => {
      if (num === -1) return;
      if (keyBoardNav) this.checkOutActiveOptionPosition();

    })
  }

  ngOnDestroy() {
    this.activeOptionSubscription.unsubscribe();

  }

  checkOutActiveOptionPosition() {
    asyncScheduler.schedule(() => {
      const activeElement = this.dropdownList.nativeElement.childNodes[this.activeOption.value.num]; //активный элемент в дропдауне
      activeElement.scrollIntoView({block: "center", behavior: "smooth"});
    }, 0);

  }
}
