import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { BehaviorSubject, Subscription } from "rxjs";

import { FormulaDataService } from "../services/formula-data.service";
import { InputDirective } from "../directives/input.directive";
import { ActiveValue } from "../interfaces/interfaces";


@Component({
  selector: 'app-formula-container',
  templateUrl: './formula-container.component.html',
  styleUrls: ['./formula-container.component.scss']
})
export class FormulaContainerComponent implements OnInit, OnDestroy {
  @ViewChild(InputDirective) inputDirective!: InputDirective;
  public formulaControl: FormControl;
  private inputControlSubscription!: Subscription;
  public activeOptionSubject!: BehaviorSubject<ActiveValue>;

  public placeholder: string = 'Введите формулу';
  public options: string[] = []; //совпадения для дропдаун меню
  public isInputFocused: boolean = false; //является ли инпут в фокусе

  public currentInputValue: string = '';

  constructor(private formulaService: FormulaDataService) {
    this.formulaControl = new FormControl('');

  }

  ngOnInit(): void {
    this.activeOptionSubject = new BehaviorSubject({num: -1});
    this.inputControlSubscription = this.formulaControl.valueChanges.subscribe((value: string) => { //подписываемся на изменения в инпуте
      this.inputDirective.inputValueChanges(value); //обрабатываем изменения в инпуте с помощью сервиса
      this.currentInputValue = this.inputDirective.currentInputValue;
      this.options = this.formulaService.findMatches(this.inputDirective.currentInputValue); //находим совпадения для дропдауна
      this.checkOutActiveOption();
    })
  }

  focus(event: FocusEvent) { //Необходимое усложнение, чтобы избежать добавление асинхронности т.к. клик по опции происходит после фокусаута.
    if (event.type === `focusout` && event.relatedTarget) return
    event.type === `focusin` ? this.isInputFocused = true : this.isInputFocused = false;

  }

  setValue(value: string) { //переписываем весь инпут
    this.inputDirective.replaceString(value, this.formulaService._functions);
    this.activeOptionSubject.next({num: -1}); //сбрасываем значение активной опции

  }

  updateValue() {// функция призвана обновлять для сервиса активное расположение каретки в инпуте
    this.formulaControl.updateValueAndValidity();

  }

  checkOutActiveOption() {//если пользователь начал выбирать опцию из дропдауна, после чего продолжил дописывать значения в инпут
    if (this.activeOptionSubject.value.num >= this.options.length) this.activeOptionSubject.next({num: this.options.length - 1});

  }

  keyupHandle(keyEvent: KeyboardEvent) {
    switch (keyEvent.key) {
      case '(':
        this.inputDirective.setStaples()
        break;

      case 'ArrowLeft':
      case 'ArrowRight':
        this.updateValue();
        break;

      case 'ArrowUp':
        keyEvent.preventDefault();
        if (this.activeOptionSubject.value.num > 0) this.activeOptionSubject.next({num: this.activeOptionSubject.value.num - 1, keyBoardNav: true});
        break;

      case 'ArrowDown':
        keyEvent.preventDefault();
        if (this.activeOptionSubject.value.num < this.options.length - 1) this.activeOptionSubject.next({num: this.activeOptionSubject.value.num + 1, keyBoardNav: true});
        break;

      case 'Enter':
        keyEvent.preventDefault();
        if (this.activeOptionSubject.value.num > -1) this.setValue(this.options[this.activeOptionSubject.value.num]);
        break;

    }
  }

  ngOnDestroy() {
    this.inputControlSubscription.unsubscribe();
    this.activeOptionSubject.unsubscribe();

  }
}
